<?php
/**
 * Top menu definition.
 *
 * @var BackendController $this
 */
$this->widget(
    'ext.yiibooster.widgets.TbNavbar',
    array(
        'type' => 'inverse',
        'brand' => 'Vektor',
        'fixed' => 'top',
        'brandUrl' => '/',
        'collapse' => true,
        'items' => array(
            array(
                'class' => 'ext.yiibooster.widgets.TbMenu',
                'type' => 'navbar',
                'items' => array(
                //    array('label' => 'Home', 'url' => array('/site/index')),
                    array('label'=>'Персоны', 'url'=>array('/person/index'),'visible' => !Yii::app()->user->isGuest),
                    array(
                        'label' => 'Продукты',
                        'visible' => !Yii::app()->user->isGuest,
                        'items' => array(
                            array('label'=>'Продукты', 'url'=>array('/product/index'),'visible' => !Yii::app()->user->isGuest),
                            array('label'=>'Подпродукты', 'url'=>array('/subproduct/index'),'visible' => !Yii::app()->user->isGuest),
                        )
                    ),
                    array('label'=>'Проекты', 'url'=>array('/project/index'),'visible' => !Yii::app()->user->isGuest),
                    array('label'=>'Фирмы', 'url'=>array('/firm/index'),'visible' => !Yii::app()->user->isGuest),
                    array('label'=>'Объекты', 'url'=>array('/object/index'),'visible' => !Yii::app()->user->isGuest),
                    array('label'=>'Территории', 'url'=>array('/territory/index'),'visible' => !Yii::app()->user->isGuest),
	            ),
            ),
            array(
                'class' => 'ext.yiibooster.widgets.TbMenu',
                'type' => 'navbar',
                'htmlOptions' => array('class' => 'pull-right'),
                'items' => array(
                //    array('label' => 'Напишите нам', 'url' => array('/site/contact')),
                    array(
                        'label' => Yii::app()->user->name,
                        'visible' => !Yii::app()->user->isGuest,
                        'items' => array(
                            array(
                                'label' => 'Пользователи',
                                'url' => array('/user'),
                                'visible' => !Yii::app()->user->isGuest
                            ),
                            array('label'=>'Права', 'url'=>array('/rights'),'visible' => !Yii::app()->user->isGuest),
                            array(
                                'label' => 'Выйти (' . Yii::app()->user->name . ')',
                                'url' => array('/site/logout'),
                                'visible' => !Yii::app()->user->isGuest
                            ),
                        )
                    ),
                    array(
                        'label' => 'Войти',
                        'url' => array('/site/login'),
                        'visible' => Yii::app()->user->isGuest
                    ),
                ),
            ),
        ),
    )
);