<?php
/**
 * Main layout file for the whole backend.
 * It is based on Twitter Bootstrap classes inside HTML5Boilerplate.
 *
 * @var BackendController $this
 * @var string $content
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl;?>/images/favicon.ico" type="image/x-icon" />
</head>

<body style="padding-top: 70px;">
<!-- NAVIGATION BEGIN -->
<?php $this->renderPartial('//layouts/_navigation');?>
<!-- NAVIGATION END -->

<!-- CONTENT WRAPPER BEGIN -->
<div class="container">
    <?php if (isset($this->breadcrumbs)): ?>
        <?php $this->widget(
            'ext.yiibooster.widgets.TbBreadcrumbs',
            array(
                'links' => $this->breadcrumbs,
            )
        ); ?>
    <?php endif?>

	<div class="row">

        <!-- CONTENT BEGIN -->
		<?= $content; ?>
        <!-- CONTENT END -->

    </div>

    <div class="row">
        <hr/>
		<footer>
			Copyright &copy; <?php echo date('Y'); ?> by Vektor.<br/>
			All Rights Reserved.<br/>
			<?= Yii::powered(); ?><br/>
            <?php echo CHtml::link('Есть вопросы? Напишите нам',array('site/contact'));?>
            <small>
                <?php
                $memory = round(Yii::getLogger()->memoryUsage/1024/1024, 3);
                $time = round(Yii::getLogger()->executionTime, 3);
                $dbStats = Yii::app()->db->getStats();
                echo '<br />Выполнено запросов: '.$dbStats[0].' (за '.round($dbStats[1], 5).' сек)';
                echo '<br />Использовано памяти: '.$memory.' МБ<br />';
                echo 'Время выполнения: '.$time.' с<br />';
                ?>
            </small>
		</footer>
	</div>

</div>
<!-- CONTENT WRAPPER END -->

</body>
</html>
