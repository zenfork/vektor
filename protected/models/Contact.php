<?php

/**
 * This is the model class for table "tbl_contact".
 *
 * The followings are the available columns in table 'tbl_contact':
 * @property string $id
 * @property string $id_person
 * @property string $id_kind_connect
 * @property string $communication_val
 *
 * The followings are the available model relations:
 * @property Person $person
 * @property KindConnect $kind_connect
 */
class Contact extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_contact';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_person, communication_val', 'required'),
			array('id_person, id_kind_connect', 'length', 'max'=>11),
			array('communication_val', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_person, id_kind_connect, communication_val', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'person'       => array(self::BELONGS_TO, 'Person', 'id_person'),
			'kind_connect' => array(self::BELONGS_TO, 'KindConnect', 'id_kind_connect'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ИД Контакта',
			'id_person' => 'ИД Персоны',
			'id_kind_connect' => 'ИД Вид связи',
			'communication_val' => 'Значение связи',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('id_person',$this->id_person,true);
		$criteria->compare('id_kind_connect',$this->id_kind_connect,true);
		$criteria->compare('communication_val',$this->communication_val,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getEditorSchema()
    {
        $kind_c_data = CHtml::listData(KindConnect::model()->findAll(),'id','kind_connect');
 //       $kind_connect_data = CHtml::listData(KindConnect::model()->findAll(),'id_kind_connect','id_kind_connect');
        $kk = array();
        $kk[] = " ";
        foreach($kind_c_data as $dat)
        {
            $kk[] = $dat;
        }
       //     print_r($kk);
      $id = Yii::app()->request->getParam('id');
    //  $model=Contact::model()->findAllByAttributes(array('id_person'=>$id));
/*            $kindConnectName = Contact::model()->findAll(array(
                'select' => 'communication_val',
                'condition' => 'id_person=:id_person',
                'params' => array(':id_person' => $id),
            ));*/
//var_dump($kindConnectName[communication_val]);
      //      $row = Contact::model()->with('kind_connect')->findAll();
        //    $qw = $row->kind_connect->KindConnect;
     //      print_r(Contact::model()->with('kind_connect')->findAll(array(
     //           'select' => 'communication_val')));

$contact = Contact::model()->findAll(array(
                'select' => 'communication_val',
				'with' => array('kind_connect'),
		//		'together' => 'true',
                'condition' => 'id_person=:id_person',
                'params' => array(':id_person' => $id),
				));
$startValArr = array();
//$c = 0;
foreach($contact as $t)
{
    $startValArr[$t->id]['communication_val'] = $t->communication_val;
    $startValArr[$t->id]['id'] = $t->id;
	$startValArr[$t->id]['id_kind_connect'] = $t->kind_connect->id;
//	$c++;
}
	$schema = array(
			'startval' => $startValArr,
            'theme' => 'bootstrap3',
            'iconlib' => 'bootstrap3',
            'disable_edit_json' => true,
            'disable_properties' => true,
            'required_by_default' => true,
            'disable_collapse' => true,
            'disable_array_reorder' => true,
            'schema' => array(
                        'type' => 'array',
        		        'format' => 'table',
                        'title' => 'Контакты',
                        'items' => array(
                            'type' => 'object',
                            'title' => 'Контакт',
          //      'enum' => array('value1','value2'),
           //                 'id' => array('efwf','egr'), // array_keys($arr),
                            'properties' => array(
                                'id' => array(
                                    'title' => 'ID контакта',
                                    'type' => 'integer',
									'options' => array(
                                        'hidden' => 'true',
                                    )
                                ),
 /*                               'id_person' => array(
                                    'type' => 'string'
                                ),*/
                                'id_kind_connect' => array(
                                    'title' => 'Вид контакта',
                                    'type' => 'string',
                                    'enum' => array_keys($kk),
									'options' => array(
										'enum_titles' => $kk
									),
                                ),
                                'communication_val' => array(
                                    'title' => 'Значения',
                                    'type' => 'string',
                                ),
                            ),
                            'default' => array(
                            ),
                        ),
            ),
        );

        return $schema;
    }
}
