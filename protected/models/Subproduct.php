<?php

/**
 * This is the model class for table "tbl_subproduct".
 *
 * The followings are the available columns in table 'tbl_subproduct':
 * @property string $id
 * @property string $name_subproduct
 * @property string $full_name_subproduct
 *
 * The followings are the available model relations:
 * @property Store[] $stores
 */
class Subproduct extends CActiveRecord
{
    public $icon; // атрибут для хранения загружаемой картинки статьи
    public $del_img; // атрибут для удаления уже загруженной картинки
    public $image; //
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_subproduct';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_subproduct', 'required'),
			array('name_subproduct', 'length', 'max'=>80),
			array('full_name_subproduct', 'safe'),
            array('del_img', 'boolean'),
            array('icon', 'file',
                'types'=>'jpg, gif, png',
                'maxSize'=>1024 * 1024 * 4, // 4 MB
                'allowEmpty'=>'true',
                'tooLarge'=>'Файл весит больше 4 MB. Пожалуйста, загрузите файл меньшего размера.',
            ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_subproduct, full_name_subproduct', 'safe', 'on'=>'search'),
        //    array('image', 'filter', 'filter'=>array("TranslitFilter", "translitFilename"))       //уже не недо, вызываем вручную
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'stores' => array(self::HAS_MANY, 'Store', 'id_subproduct'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ИД Субпродукта',
			'name_subproduct' => 'Имя субпродукта',
			'full_name_subproduct' => 'Полное название',
            'icon' => 'Картинка к статье',
            'del_img'=>'Удалить картинку?',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name_subproduct',$this->name_subproduct,true);
		$criteria->compare('full_name_subproduct',$this->full_name_subproduct,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array('pageSize'=>50)
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subproduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getImageThumb($title, $image, $width='150'){
        if(isset($image) && file_exists($_SERVER['DOCUMENT_ROOT'].
                Yii::app()->urlManager->baseUrl.
                '/images/thumbs_small/subproduct/'.$image))
            return(Yii::app()->getBaseUrl(true).'/images/thumbs_small/subproduct/'.$image);
        else
            return(Yii::app()->getBaseUrl(true).'/images/no-thumb.png');
    }
}
