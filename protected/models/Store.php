<?php

/**
 * This is the model class for table "tbl_store".
 *
 * The followings are the available columns in table 'tbl_store':
 * @property string $id
 * @property string $id_product
 * @property string $id_subproduct
 * @property integer $number
 *
 * The followings are the available model relations:
 * @property Product $product
 * @property Subproduct $subproduct
 */
class Store extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_store';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_product, id_subproduct, number', 'required'),
			array('number', 'numerical', 'integerOnly'=>true),
			array('id_product, id_subproduct', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_product, id_subproduct, number', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product'    => array(self::BELONGS_TO, 'Product', 'id_product'),
			'subproduct' => array(self::BELONGS_TO, 'Subproduct', 'id_subproduct'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ИД Склада',
			'id_product' => 'ИД Продукта',
			'id_subproduct' => 'ИД Субпродукта',
			'number' => 'Количество',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('id_product',$this->id_product,true);
		$criteria->compare('id_subproduct',$this->id_subproduct,true);
		$criteria->compare('number',$this->number);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Store the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    public static function getEditorSchema()
    {
        $enum[] = " ";
        $enum_titles[] = " ";
        $listData = CHtml::listData(Subproduct::model()->findAll(array('order'=>'name_subproduct ASC')),'id','name_subproduct');
        foreach($listData as $id=>$dat)
        {
            $enum[] = $id;
            $enum_titles[] = $dat;
        }
        $id = Yii::app()->request->getParam('id');
        $storeModel = Store::model()->findAll(array(
            'select' => 'number',
            'with' => array('subproduct'),
            //		'together' => 'true',
            'condition' => 'id_product=:id_product',
            'params' => array(':id_product' => $id),
        ));
        $startValArr = array();
        foreach($storeModel as $t)
        {
            $startValArr[$t->id]['id'] = $t->id;
            $startValArr[$t->id]['id_subproduct'] = $t->subproduct->id;
            $startValArr[$t->id]['number'] = $t->number;
        }
        $schema = array(
            'startval' => $startValArr,
            'theme' => 'bootstrap3',
            'iconlib' => 'bootstrap3',
            'disable_edit_json' => true,
            'disable_properties' => true,
            'required_by_default' => true,
            'disable_collapse' => true,
            'disable_array_reorder' => true,
            'schema' => array(
                'type' => 'array',
                'format' => 'table',
                'title' => 'Субпродукты',
                'items' => array(
                    'type' => 'object',
                    'title' => 'Субпродукт',
                    'properties' => array(
                        'id' => array(
                            'title' => 'ID субпродукта',
                            'type' => 'number',
                            'input_width' => '40px',
                            'options' => array(
                                'hidden' => 'true',
                            )
                        ),
                        'id_subproduct' => array(
                            'title' => 'Субпродукт',
                            'type' => 'string',
                            'enum' => $enum,
                            'options' => array(
                                'enum_titles' => $enum_titles
                            ),
                        ),
                        'number' => array(
                            'title' => 'Количество',
                            'type' => 'string',
                        )
                    ),
                    'default' => array(
                    ),
                ),
            ),
        );

        return $schema;
    }
}
