<?php

/**
 * This is the model class for table "tbl_project".
 *
 * The followings are the available columns in table 'tbl_project':
 * @property string $id
 * @property string $id_object
 * @property string $stamp
 * @property string $status
 * @property string $id_responsible
 * @property string $id_designer
 * @property integer $redesign
 * @property string $id_firm
 * @property string $id_file
 *
 * The followings are the available model relations:
 * @property Object $object
 * @property Person $responsible
 * @property Person $designer
 * @property Specification[] $specifications
 */
class Project extends CActiveRecord
{
    public $name_object, $address_object, $person_responsible, $person_designer, $name_firm, $id_territory;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_project';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_object, id_responsible, id_designer, id_firm', 'required'),
			array('redesign', 'numerical', 'integerOnly'=>true),
			array('id_object, id_responsible, id_designer, id_firm', 'length', 'max'=>11),
			array('stamp, status', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_object, stamp, status, id_responsible, id_designer, redesign, id_firm, name_object,
			 address_object, person_responsible, person_designer, name_firm, id_territory', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'object'          => array(self::BELONGS_TO, 'Object', 'id_object'),
			'responsible'     => array(self::BELONGS_TO, 'Person', 'id_responsible'),
			'designer'        => array(self::BELONGS_TO, 'Person', 'id_designer'),
			'specifications'  => array(self::HAS_MANY, 'Specification', 'id_project'),
            'firm'            => array(self::BELONGS_TO, 'Firm', 'id_firm'),
            'territory'       => array(self::BELONGS_TO, 'Territory', array('id_territory'=>'id'), 'through'=>'firm'),
            'product'         => array(self::BELONGS_TO, 'Product', 'id_product', 'through'=>'specifications'),
            'stores'          => array(self::HAS_MANY, 'Store', array('id'=>'id_product'), 'through'=>'product'),
            'subproduct'      => array(self::BELONGS_TO, 'Subproduct', 'id_subproduct', 'through'=>'stores'),
            'project_files'   => array(self::HAS_MANY, 'ProjectFile', 'id_project'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ИД Проекта',
			'id_object' => 'Объект',
            'address_object' => 'Адрес объекта',
			'stamp' => 'Штамп',
			'status' => 'Статус',
			'id_responsible' => 'Ответственный',
			'id_designer' => 'Разработчик',
			'redesign' => 'Перепроектирование',
			'id_firm' => 'Фирма',
            'id_territory' => 'Территория',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('id_object',$this->id_object,true);
		$criteria->compare('stamp',$this->stamp,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('id_responsible',$this->id_responsible,true);
		$criteria->compare('id_designer',$this->id_designer,true);
		$criteria->compare('redesign',$this->redesign);
		$criteria->compare('t.id_firm',$this->id_firm,true);
        $criteria->with = array('object','responsible','designer','firm','territory');
        $criteria->compare('CONCAT_WS(" ", responsible.first_name, responsible.last_name, responsible.surname)',
            $this->person_responsible, true); // чот не робит
        $criteria->compare('CONCAT_WS(" ", designer.first_name, designer.last_name, designer.surname)',
            $this->person_designer, true); // чот не робит [2]
        $criteria->compare('object.name_object', $this->name_object, true);
        $criteria->compare('object.address_object', $this->address_object, true);
 //     $criteria->compare('responsible.last_name', $this->person_responsible, true);
 //     $criteria->compare('designer.last_name', $this->person_designer, true);
        $criteria->compare('firm.name_firm', $this->name_firm, true);
        $criteria->compare('territory.id', $this->id_territory, true);

        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>array(
                'attributes'=>array(
                    'name_object'=>array(
                        'asc'=>'object.name_object',
                        'desc'=>'object.name_object DESC',
                    ),
                    'address_object'=>array(
                        'asc'=>'object.address_object',
                        'desc'=>'object.address_object DESC',
                    ),
                    'person_responsible'=>array(
                        'asc'=>'CONCAT_WS(" ", responsible.first_name, responsible.last_name, responsible.surname)',
                        'desc'=>'CONCAT_WS(" ", responsible.first_name, responsible.last_name, responsible.surname) DESC',
                    ),
                    'person_designer'=>array(
                        'asc'=>'CONCAT_WS(" ", designer.first_name, designer.last_name, designer.surname)',
                        'desc'=>'CONCAT_WS(" ", designer.first_name, designer.last_name, designer.surname) DESC',
                    ),
                    'name_firm'=>array(
                        'asc'=>'firm.name_firm',
                        'desc'=>'firm.name_firm DESC',
                    ),
                    'id_territory'=>array(
                        'asc'=>'territory.name_territory',
                        'desc'=>'territory.name_territory DESC',
                    ),
                    '*',
                ),
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    function getClients()
    {
        $Persons = Person::model()->findAll();
        $list    = CHtml::listData($Persons , 'id', 'fullName');
        return $list;
    }

    public function getStatus($status)
    {
        $data = array(0=>"Р", 1=>"Н");
        return $data[$status];
    }

    public function getRedesign($redesign)
    {
        $data = array(0=>"нет", 1=>"да");
        return $data[$redesign];
    }
}