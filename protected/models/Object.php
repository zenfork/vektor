<?php

/**
 * This is the model class for table "tbl_object".
 *
 * The followings are the available columns in table 'tbl_object':
 * @property string $id
 * @property string $name_object
 * @property string $type_object
 * @property string $desc_object
 * @property string $address_object
 * @property string $id_developer
 *
 * The followings are the available model relations:
 * @property Firm $developer
 * @property Projects[] $projects
 */
class Object extends CActiveRecord
{
    public $firm_search;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_object';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_object, type_object, address_object, id_developer', 'required'),
			array('name_object, address_object', 'length', 'max'=>255),
			array('type_object', 'length', 'max'=>100),
			array('id_developer', 'length', 'max'=>11),
			array('desc_object', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_object, type_object, desc_object, address_object, id_developer, firm_search', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'developer' => array(self::BELONGS_TO, 'Firm', 'id_developer'),
			'projects' => array(self::HAS_MANY, 'Project', 'id_object'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ИД Объекта',
			'name_object' => 'Название объекта',
			'type_object' => 'Тип объект',
			'desc_object' => 'Описание объекта',
			'address_object' => 'Адрес объекта',
			'id_developer' => 'Фирма-разработчик',
            'firm_search' => 'Фирма',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name_object',$this->name_object,true);
		$criteria->compare('type_object',$this->type_object,true);
		$criteria->compare('desc_object',$this->desc_object,true);
		$criteria->compare('address_object',$this->address_object,true);
		$criteria->compare('id_developer',$this->id_developer,true);
        $criteria->with = array('developer');
        $criteria->compare( 'developer.name_firm', $this->firm_search, true );

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>array(
                'attributes'=>array(
                    'firm_search'=>array(
                        'asc'=>'developer.name_firm',
                        'desc'=>'developer.name_firm DESC',
                    ),
                    '*',
                ),
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Object the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
