<?php

/**
 * This is the model class for table "tbl_specification".
 *
 * The followings are the available columns in table 'tbl_specification':
 * @property string $id
 * @property string $id_project
 * @property string $id_product
 * @property integer $number
 *
 * The followings are the available model relations:
 * @property Projects $project
 * @property Product $product
 */
class Specification extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_specification';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_project, id_product', 'required'),
			array('number', 'numerical', 'integerOnly'=>true),
			array('id_project, id_product', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_project, id_product, number', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Project', 'id_project'),
			'product' => array(self::BELONGS_TO, 'Product', 'id_product'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ИД Спецификации',
			'id_project' => 'ИД Проекта',
			'id_product' => 'ИД Продукта',
			'number' => 'Количество',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('id_project',$this->id_project,true);
		$criteria->compare('id_product',$this->id_product,true);
		$criteria->compare('number',$this->number);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Specification the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getEditorSchema()
    {
        $enum[] = " ";
        $enum_titles[] = " ";
        $listData = CHtml::listData(Product::model()->findAll(array('order'=>'name_product ASC')),'id','name_product');
        foreach($listData as $id=>$dat)
        {
            $enum[] = $id;
            $enum_titles[] = $dat;
        }
        $id = Yii::app()->request->getParam('id');
        $specification = Specification::model()->findAll(array(
            'select' => 'number',
            'with' => array('product'),
            //		'together' => 'true',
            'condition' => 'id_project=:id_project',
            'params' => array(':id_project' => $id),
        ));
        $startValArr = array();
        foreach($specification as $t)
        {
            $startValArr[$t->id]['number'] = $t->number;
            $startValArr[$t->id]['id'] = $t->id;
            $startValArr[$t->id]['id_product'] = $t->product->id;
        }
        $schema = array(
            'startval' => $startValArr,
            'theme' => 'bootstrap3',
            'iconlib' => 'bootstrap3',
            'disable_edit_json' => true,
            'disable_properties' => true,
            'required_by_default' => true,
            'disable_collapse' => true,
            'disable_array_reorder' => true,
            'schema' => array(
                'type' => 'array',
                'format' => 'table',
                'title' => 'Продукты',
                'items' => array(
                    'type' => 'object',
                    'title' => 'Продукт',
                    'properties' => array(
                        'id' => array(
                            'title' => 'ID продукта',
                            'type' => 'number',
                            'input_width' => '40px',
                            'options' => array(
                                'hidden' => 'true',
                            )
                        ),
                        'id_product' => array(
                            'title' => 'Субпродукт',
                            'type' => 'string',
                            'enum' => $enum,
                            'options' => array(
                                'enum_titles' => $enum_titles
                            ),
                        ),
                        'number' => array(
                            'title' => 'Количество',
                            'type' => 'string',
                        )
                    ),
                    'default' => array(
                    ),
                ),
            ),
        );

        return $schema;
    }

}
