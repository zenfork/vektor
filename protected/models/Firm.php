<?php

/**
 * This is the model class for table "tbl_firm".
 *
 * The followings are the available columns in table 'tbl_firm':
 * @property string $id
 * @property string $name_firm
 * @property integer $dealer
 * @property string $id_territory
 * @property string $firm_address
 * @property string $edrpou
 *
 * The followings are the available model relations:
 * @property Territory $territory
 * @property Object[] $objects
 * @property Person[] $peoples
 */
class Firm extends CActiveRecord
{
    public $name_territory;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_firm';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_firm, dealer, id_territory, firm_address', 'required'),
			array('dealer', 'numerical', 'integerOnly'=>true),
			array('id, id_territory', 'length', 'max'=>11),
			array('name_firm', 'length', 'max'=>80),
			array('firm_address', 'length', 'max'=>255),
			array('edrpou', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_firm, dealer, id_territory, firm_address, edrpou, name_territory', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'territory' => array(self::BELONGS_TO, 'Territory', 'id_territory'),
			'objects'   => array(self::HAS_MANY, 'Object', 'id_developer'),
			'peoples'   => array(self::HAS_MANY, 'Person', 'id_firm'),
            'projects'  => array(self::HAS_MANY, 'Project', 'id_firm'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ИД Фирмы',
			'name_firm' => 'Имя фирмы',
			'dealer' => 'Дилер',
			'id_territory' => 'Территория',
			'firm_address' => 'Адрес фирмы',
			'edrpou' => 'ЕДРПОУ',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name_firm',$this->name_firm,true);
		$criteria->compare('dealer',$this->dealer);
		$criteria->compare('id_territory',$this->id_territory,true);
		$criteria->compare('firm_address',$this->firm_address,true);
		$criteria->compare('edrpou',$this->edrpou,true);
        $criteria->with = array('territory');
        $criteria->compare('territory.name_territory', $this->name_territory, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array('pageSize'=>25),
            'sort'=>array(
                'attributes'=>array(
                    'person_responsible'=>array(
                        'asc'=>'CONCAT_WS(" ", responsible.first_name, responsible.last_name, responsible.surname)',
                        'desc'=>'CONCAT_WS(" ", responsible.first_name, responsible.last_name, responsible.surname) DESC',
                    ),
                    'person_designer'=>array(
                        'asc'=>'CONCAT_WS(" ", designer.first_name, designer.last_name, designer.surname)',
                        'desc'=>'CONCAT_WS(" ", designer.first_name, designer.last_name, designer.surname) DESC',
                    ),
                    'name_territory'=>array(
                        'asc'=>'territory.name_territory',
                        'desc'=>'territory.name_territory DESC',
                    ),
                    '*',
                ),
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Firm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
