<?php

/**
 * This is the model class for table "tbl_person".
 *
 * The followings are the available columns in table 'tbl_person':
 * @property string $id
 * @property string $last_name
 * @property string $first_name
 * @property string $surname
 * @property string $id_firm
 * @property string $id_position
 * @property integer $sex
 *
 * The followings are the available model relations:
 * @property Contact[] $contacts
 * @property Firm $firm
 * @property Position $position
 * @property responsibles[] $projects
 * @property designers[] $projects1
 */
class Person extends CActiveRecord
{
    public $firm_search, $position_search, $fullname;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_person';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('last_name, first_name, surname, sex', 'required'),
			array('sex', 'numerical', 'integerOnly'=>true),
			array('last_name, first_name, surname', 'length', 'max'=>60),
			array('id_firm, id_position', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, last_name, first_name, surname, id_firm, id_position, sex, firm_search, position_search, fullname', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contacts'     => array(self::HAS_MANY, 'Contact', 'id_person'),
			'firm'         => array(self::BELONGS_TO, 'Firm', 'id_firm'),
			'position'     => array(self::BELONGS_TO, 'Position', 'id_position'),
			'responsibles' => array(self::HAS_MANY, 'Project', 'id_responsible'), //projects
			'designers'    => array(self::HAS_MANY, 'Project', 'id_designer'), //projects1
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id Person',
			'last_name' => 'Фамилия',
			'first_name' => 'Имя',
			'surname' => 'Отчество',
			'id_firm' => 'Фирма',
			'id_position' => 'Должность',
			'sex' => 'Пол',
            'fullname' => 'Полное имя'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('surname',$this->surname,true);
    //    $criteria->compare('last_name',$this->full_name,true, 'OR');
    //    $criteria->compare('first_name',$this->full_name,true, 'OR');
    //    $criteria->compare('surname',$this->full_name,true, 'OR');
        $criteria->compare('CONCAT_WS(" ", last_name, first_name, surname)', $this->fullname, true);
        $criteria->compare('id_firm',$this->id_firm,true);
		$criteria->compare('id_position',$this->id_position,true);
		$criteria->compare('sex',$this->sex);
        $criteria->with = array('firm', 'position');
        $criteria->compare( 'firm.name_firm', $this->firm_search, true );
        $criteria->compare( 'position.name_position', $this->position_search, true );

        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array('pageSize'=>25),
            'sort'=>array(
                'attributes'=>array(
                    'firm_search'=>array(
                        'asc'=>'firm.name_firm',
                        'desc'=>'firm.name_firm DESC',
                    ),
                    'position_search'=>array(
                        'asc'=>'position.name_position',
                        'desc'=>'position.name_position DESC',
                    ),
                    'fullname'=>array(
                        'asc'=>'CONCAT_WS(" ", last_name, first_name, surname)',
                        'desc'=>'CONCAT_WS(" ", last_name, first_name, surname) DESC',
                    ),
                    '*',
                ),
            ),
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Person the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getFullName()
	{
        if ($this->first_name !== null && $this->last_name !== null && $this->surname !== null) {
            $this->FullName = $this->last_name . ' ' . $this->first_name . ' ' . $this->surname;
        }
        return $this->FullName;
	}

    public function setFullName($value)
    {
        $this->FullName = $value;
    }
}
