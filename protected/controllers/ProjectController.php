<?php

class ProjectController extends Controller
{
    public function actions()
    {
        return array(
            'toggleredesign' => array(
                'class'=>'ext.yiibooster.actions.TbToggleAction',
                'modelName' => 'Project',
            )
        );
    }
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
		//	'accessControl', // perform access control for CRUD operations
            'rights',
			'postOnly + delete', // we only allow deletion via POST request
            array('ext.yiibooster.filters.BoosterFilter - delete')
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','relational'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','toggleredesign'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Project;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Project']))
		{
			$model->attributes=$_POST['Project'];
            if($model->save())
            {
                $this->jsonEditorSaveModel($model);
                $this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Project']))
		{
			$model->attributes=$_POST['Project'];
            // THIS is how you capture those uploaded images: remember that in your CMultiFile widget, you set 'name' => 'images'
            $images = CUploadedFile::getInstancesByName('file');
            // proceed if the images have been set
            if (isset($images) && count($images) > 0) {
                // go through each uploaded image
                $folder=Yii::getPathOfAlias('webroot.documents.'.Yii::app()->controller->id.'.'.$id).DIRECTORY_SEPARATOR;
                // create folder with ID project
                if(!is_dir($folder)){
                    mkdir($folder);
                }
                foreach ($images as $image => $pic) {
                //    echo $pic->name.'<br />';
                    if ($pic->saveAs($folder.$pic->name)) {
                        // add it to the main model now
                        $img_add = new ProjectFile();
                        $img_add->name_file = $pic->name; //it might be $img_add->name for you, filename is just what I chose to call it in my model
                        $img_add->id_project = $model->id; // this links your picture model to the main model (like your user, or profile model)

                        $img_add->save(); // DONE
                    }
                        // else: handle the errors here, if you want
                }
		    }
            if($model->save())
            {
                $this->jsonEditorSaveModel($model);
                $this->redirect(array('view','id'=>$model->id));
            }
        }

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
/*		$dataProvider=new CActiveDataProvider('Project');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
        $model=new Project('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Project']))
            $model->attributes=$_GET['Project'];

        $this->render('index',array(
            'model'=>$model,
        ));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Project('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Project']))
			$model->attributes=$_GET['Project'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Project the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Project::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Project $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='project-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


    // relational, isn't it?
    public function actionRelational()
    {
// Get rawData and create dataProvider
      //  $rawData=Store::model()->findAll();

        $rawData=Object::model()->findByPk(Yii::app()->getRequest()->getParam('id'));
        $rawData=$rawData->projects;
        //       $product=$store->product; //(array('condition'=>'status=1'))
        //       $producte=$store->product->stores->number;
 //       $store=$spec->with('stores');
 //       $subprod=$store->with('subproducts')->findAll();


        $dataProvider=new CArrayDataProvider($rawData, array(
            'id'=>'id',
            /* 'sort'=>array(
                'attributes'=>array(
                    'username', 'email',
                ),
            ), */
            'pagination'=>false
            // wtf    'totalItemCount'=>false
        ));
        // partially rendering "_relational" view
        $this->renderPartial('_relational', array(
            'id' => Yii::app()->getRequest()->getParam('id'),
            'gridDataProvider' => $dataProvider,
         //   'gridColumns' => array('id','name_product')
        ), false, true);
    }

    private function jsonEditorSaveModel($model) {
        $specifications = $_POST['root'];

        // i don't know how to do UPDATE :c
        Specification::model()->deleteAll('id_project=:id_project',array(':id_project'=>$model->id));

        if ($specifications) {
            foreach ($specifications as $specification) {
                //   	$modelSpecification=Store::model()->findByPk($specification['id']);
                $modelSpecification = new Specification;
                $modelSpecification->id = $specification['id'];
                $modelSpecification->id_project = $model->id;
                if (isset($specification['id_product'])) {
                    $modelSpecification->id_product = $specification['id_product'];
                } else {
                    $modelSpecification->id_product = "";
                }
                $modelSpecification->number = $specification['number'];
                $modelSpecification->save();
            }
        }
    }
}
