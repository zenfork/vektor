<?php

class SubproductController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
            //	'accessControl', // perform access control for CRUD operations
            'rights',
			'postOnly + delete', // we only allow deletion via POST request
            array('ext.yiibooster.filters.BoosterFilter - delete')
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','relational'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
    public function actionCreate()
    {
        $model=new Subproduct;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Subproduct']))
        {
            $model->attributes=$_POST['Subproduct'];
            $model->icon=CUploadedFile::getInstance($model,'icon'); //Атрибуту icon присвоить указатель на загружаемый файл
            if($model->save()){ //Если поле загрузки файла не было пустым, то
                if ($model->icon){ //сохранить файл на сервере в каталог images/2013 под именем  //month-day-alias.jpg
                    $sourcePath = pathinfo($model->icon->getName());
                    $fileName = TranslitFilter::translitFilename($model->id.'_'.$model->name_subproduct.'_'.date('d-m-y').'.'.$sourcePath['extension']);
                    $model->image = $fileName;
                    $pathName = $_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/images/';
                    $file = $pathName.'/'.Yii::app()->controller->id.'/'.$model->image; //Переменной $file присвоить путь, куда сохранится картинка без изменений
                    $model->icon->saveAs($file); //Используем функции расширения CImageHandler;
                    $model->save();
                    $ih = new CImageHandler(); //Инициализация
                    Yii::app()->ih
                        ->load($file) //Загрузка оригинала картинки
                        ->thumb('200', false) //Создание превьюшки шириной 200px
                        ->save($pathName.'/thumbs/'.Yii::app()->controller->id.'/'.$fileName) //Сохранение превьюшки в папку thumbs
                        ->reload() //Снова загрузка оригинала картинки
                        ->thumb('50', '50') //Создание превьюшки размером 50px
                        ->save($pathName.'/thumbs_small/'.Yii::app()->controller->id.'/'.$fileName) //Сохранение превьюшки в папку thumbs_small
                        ->reload()//Снова загрузка оригинала картинки
                        ->thumb('800', '800') //Создание превьюшки размером 800px
                        ->save($pathName.'/'.Yii::app()->controller->id.'/'.$fileName) //Сохранение большой картинки в папку
                    ;
                }
                $this->redirect(array('view','id'=>$model->id));
            }
        }
        $this->render('create',array( 'model'=>$model, ));
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Subproduct']))
        {
            $model->attributes=$_POST['Subproduct'];
            $model->icon=CUploadedFile::getInstance($model,'icon'); //Атрибуту icon присвоить указатель на загружаемый файл
            if($model->save()){ //Если поле загрузки файла не было пустым, то
                if($model->del_img){ //Если отмечен чекбокс «удалить файл»
                    if(file_exists(Yii::getPathOfAlias('webroot.images.'.Yii::app()->controller->id).DIRECTORY_SEPARATOR.$model->image)){
                        //удаляем картинку и все её thumbs
                        @unlink('./images/'.Yii::app()->controller->id.'/'.$model->image);
                        @unlink('./images/thumbs/'.Yii::app()->controller->id.'/'.$model->image);
                        @unlink('./images/thumbs_small/'.Yii::app()->controller->id.'/'.$model->image);
                        $model->image = null;
                    }
                }
                if ($model->icon){ //сохранить файл на сервере в каталог images/2013 под именем  //month-day-alias.jpg
                    $sourcePath = pathinfo($model->icon->getName());
                    $fileName = TranslitFilter::translitFilename($model->id.'_'.$model->name_subproduct.'_'.date('d-m-y').'.'.$sourcePath['extension']);
                    $model->image = $fileName;
                    $pathName = $_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/images';
                    $file = $pathName.'/'.Yii::app()->controller->id.'/'.$fileName; //Переменной $file присвоить путь, куда сохранится картинка без изменений
                    $model->icon->saveAs($file); //Используем функции расширения CImageHandler;
                    $ih = new CImageHandler(); //Инициализация
                    Yii::app()->ih
                        ->load($file) //Загрузка оригинала картинки
                        ->thumb('200', false) //Создание превьюшки шириной 200px
                        ->save($pathName.'/thumbs/'.Yii::app()->controller->id.'/'.$fileName) //Сохранение превьюшки в папку thumbs
                        ->reload() //Снова загрузка оригинала картинки
                        ->thumb('50', '50') //Создание превьюшки размером 50px
                        ->save($pathName.'/thumbs_small/'.Yii::app()->controller->id.'/'.$fileName) //Сохранение превьюшки в папку thumbs_small
                        ->reload()//Снова загрузка оригинала картинки
                        ->thumb('800', '800') //Создание превьюшки размером 800px
                        ->save($pathName.'/'.Yii::app()->controller->id.'/'.$fileName) //Сохранение большой картинки в папку
                    ;
                }
                $model->save();
                $this->redirect(array('view','id'=>$model->id));
            }
        }
        $this->render('update',array( 'model'=>$model, ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
/*		$dataProvider=new CActiveDataProvider('Subproduct');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
        $model=new Subproduct('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Subproduct']))
            $model->attributes=$_GET['Subproduct'];

        $this->render('index',array(
            'model'=>$model,
        ));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Subproduct('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Subproduct']))
			$model->attributes=$_GET['Subproduct'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Subproduct the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Subproduct::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Subproduct $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='subproduct-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function post_image($title, $image, $width='150', $class='post_img'){
        if(isset($image) && file_exists($_SERVER['DOCUMENT_ROOT'].
                Yii::app()->urlManager->baseUrl.
                '/images/subproduct/'.$image))
            return CHtml::image(Yii::app()->getBaseUrl(true).'/images/subproduct/'.$image, $title,
                array(
                    'width'=>$width,
                    'class'=>$class,
                )
            );
        else
            return CHtml::image(Yii::app()->getBaseUrl(true).'/images/no-thumb.png','Нет картинки',
                array(
                    'width'=>$width,
                    'class'=>$class
                )
            );
    }
}
