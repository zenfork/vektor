<?php

class StoreController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
            //	'accessControl', // perform access control for CRUD operations
            'rights',
			'postOnly + delete', // we only allow deletion via POST request
            array('ext.yiibooster.filters.BoosterFilter - delete')
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','relational'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Store;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Store']))
		{
			$model->attributes=$_POST['Store'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Store']))
		{
			$model->attributes=$_POST['Store'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Store');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Store('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Store']))
			$model->attributes=$_GET['Store'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Store the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Store::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Store $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='store-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    // relational tables
    public function actionRSubproduct()
    {
// Get rawData and create dataProvider
        $rawData=Product::model()->findByPk(Yii::app()->getRequest()->getParam('id'));
        $rawData=$rawData->stores;
 //       $product=$store->product; //(array('condition'=>'status=1'))
 //       $producte=$store->product->stores->number;
  //      $store=$spec->with('stores');
  //      $subprod=$store->with('subproducts')->findAll();

  //      var_dump($subprod);

        $dataProvider=new CArrayDataProvider($rawData, array(
/*            'id'=>'ID',
             'sort'=>array(
                'attributes'=>array(
                    'id', 'subproduct.name_subproduct',
                ),
            ),*/
            'pagination'=>false
            // wtf    'totalItemCount'=>false
        ));
        // partially rendering "_relational" view
        $this->renderPartial('_rsubproduct', array(
            'id' => Yii::app()->getRequest()->getParam('id'),
            'gridDataProvider' => $dataProvider,
            'gridColumns' => array(
             //   'id',
                array( 'class'=>'booster.widgets.TbImageColumn',
                    'imagePathExpression'=>'$data->subproduct->getImageThumb($data->subproduct->name_subproduct, $data->subproduct->image)',
                    'headerHtmlOptions'=>array('style'=>'min-width: 64px;'),
                    'imageOptions'=>array('width'=>'64px')
                ),
                array('name'=>'Имя субпродукта', 'value'=>'$data->subproduct->name_subproduct'),
//                'subproduct.name_subproduct',
                array('name'=>'Полное имя субпродукта', 'value'=>'$data->subproduct->full_name_subproduct'),
                array('name'=>'Количество', 'value'=>'$data->number'),
            )
        ));
    }

    public function actionRProduct() // для относительная
    {
// Get rawData and create dataProvider
        $rawData=Subproduct::model()->findByPk(Yii::app()->getRequest()->getParam('id'));
        $rawData=$rawData->stores;
        $dataProvider=new CArrayDataProvider($rawData, array(
            'pagination'=>false
        ));
        // partially rendering "_relational" view
        $this->renderPartial('_rproduct', array(
            'id' => Yii::app()->getRequest()->getParam('id'),
            'gridDataProvider' => $dataProvider,
            'gridColumns' => array(
                //   'id',
                array('name'=>'Имя продукта', 'value'=>'$data->product->name_product'),
                array('name'=>'Количество', 'value'=>'$data->number'),
            )
        ));
    }
}
