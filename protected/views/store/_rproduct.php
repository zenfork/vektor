<?php
/* @var $this productController */
/* @var $data product */
?>

<?php
if(Yii::app()->user->checkAccess('Store.Delete')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn',
        'template'=>'{delete}'
    );
}

$this->widget('ext.yiibooster.widgets.TbExtendedGridView', array(
    'id'=>'product-grid-'.$id,
    'type'=>'striped bordered',
    'dataProvider'=>$gridDataProvider,
    'template' => "{items}",
    'columns' => array_merge(
        $gridColumns,
        $columns
    ),
));
?>