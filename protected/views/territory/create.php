<?php
/* @var $this TerritoryController */
/* @var $model Territory */

$this->breadcrumbs=array(
	'Территории'=>array('index'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список территорий', 'url'=>array('index')),
	array('label'=>'Управление территориями', 'url'=>array('admin')),
);
?>

<h1>Создать территорию</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>