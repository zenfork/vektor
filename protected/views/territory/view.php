<?php
/* @var $this TerritoryController */
/* @var $model Territory */

$this->breadcrumbs=array(
	'Территории'=>array('index'),
	$model->name_territory,
);

$this->menu=array(
	array('label'=>'Список территорий', 'url'=>array('index')),
	array('label'=>'Создать территорию', 'url'=>array('create')),
	array('label'=>'Обновить территорию', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить территорию', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление территориями', 'url'=>array('admin')),
);
?>

<h1>Просмотр территории "<?php echo $model->name_territory; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name_territory',
	),
)); ?>
