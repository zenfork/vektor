<?php
/* @var $this TerritoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Территории',
);

$this->menu=array(
	array('label'=>'Создать территорию', 'url'=>array('create')),
	array('label'=>'Управление территориями', 'url'=>array('admin')),
);
?>

<h1>Территории</h1>

<?php
$columns=array
(
   // 'id',
);

if(Yii::app()->user->checkAccess('Subproduct.View') && !Yii::app()->user->checkAccess('Subproduct.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn',
        'template'=>'{view}'
    );
}
if(Yii::app()->user->checkAccess('Subproduct.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn'
    );
}

$this->widget('ext.yiibooster.widgets.TbExtendedGridView', array(
    'filter'=>$model,
    'type'=>'striped bordered',
    'dataProvider' => $model->search(),
    'template' => "{summary}{pager}{items}{pager}",
    'columns' => array_merge(array(
        array(
            'class'=>'ext.yiibooster.widgets.TbRelationalColumn',
            'name' => 'name_territory',
            'url' => $this->createUrl('firm/relational'),
            'value'=> '$data->name_territory'
        )
    ),
        $columns
    ),
));
?>
