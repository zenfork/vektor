<?php
/* @var $this TerritoryController */
/* @var $model Territory */

$this->breadcrumbs=array(
	'Территории'=>array('index'),
	$model->name_territory=>array('view','id'=>$model->id),
	'Обновить',
);

$this->menu=array(
	array('label'=>'Список территорий', 'url'=>array('index')),
	array('label'=>'Создать территорию', 'url'=>array('create')),
	array('label'=>'Просмотр территории', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление территориями', 'url'=>array('admin')),
);
?>

<h1>Обновить территорию "<?php echo $model->name_territory; ?>"</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>