<?php
/* @var $this TerritoryController */
/* @var $data Territory */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_territory')); ?>:</b>
	<?php echo CHtml::encode($data->name_territory); ?>
	<br />


</div>