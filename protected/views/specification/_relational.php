<?php
/* @var $this SubproductController */
/* @var $data Subproduct */

/*$this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'product-grid-'.$id,
            'dataProvider'=>$gridDataProvider,
            'filter'=>$model,
            'columns'=>array(
                'id',
                'product.name_product',
                'number',
                array(
                    'class'=>'CButtonColumn',
                ),
            ),
    ));*/


if(Yii::app()->user->checkAccess('Specification.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn',
        'template'=>'{delete}'
    );
}

$this->widget('ext.yiibooster.widgets.TbExtendedGridView', array(
    'id'=>'product-grid-'.$id,
    'type'=>'striped bordered',
    'dataProvider'=>$gridDataProvider,
    'template' => "{summary}{pager}{items}{pager}",
    'columns' => array_merge(
    /* array(
   array(
        'class'=>'ext.yiibooster.widgets.TbRelationalColumn',
        'htmlOptions'=>array('class'=>'tbrelational-column-specification'),
        'name' => 'firstLetter',
        'url' => $this->createUrl('product/relational'),
        'value'=> '$data->product->name_product'


    ),)*/
        $gridColumns,
        $columns
    ),
));