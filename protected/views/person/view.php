<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'Персоны'=>array('index'),
	$model->getFullName(),
);

$this->menu=array(
	array('label'=>'Список персон', 'url'=>array('index')),
	array('label'=>'Создать персону', 'url'=>array('create')),
	array('label'=>'Обновить персону', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить персону', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),
        'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление персонами', 'url'=>array('admin')),
);
?>

<h1>Просмотр персоны "<?php echo $model->getFullName(); ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
	//	'id',
		'last_name',
		'first_name',
		'surname',
        array( 'label'=>'Фирма', 'value'=>$model->firm->name_firm ),
        array( 'label'=>'Должность', 'value'=>$model->position->name_position ),
        [
            'name' => 'sex',
            'format'=>'raw',
            'value' => $model->sex == 0 ? 'мужской' : 'женский'
        ],
        array(
            'label'=>'Контакты',
            'type'=>'raw',
            'value'=>$this->renderPartial('_contacts', array('model'=>$model), true),
        ),
	),
));?>
