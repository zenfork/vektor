<?php
/* @var $this PersonController */
/* @var $model Person */
/* @var $form CActiveForm */
?>
<?php
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/jsoneditor.min.js');
$ContactSchema = json_encode(Contact::getEditorSchema()); // ContactScheme = ... getPersonSchema
?>
<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'person-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model,'last_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'.col-md-4','maxlength'=>60)))); ?>

    <?php echo $form->textFieldGroup($model,'first_name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'.col-md-4','maxlength'=>60)))); ?>

    <?php echo $form->textFieldGroup($model,'surname',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'.col-md-4','maxlength'=>60)))); ?>

    <?php echo $form->dropDownListGroup(
        $model,
        'id_firm',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'widgetOptions' => array(
                'data' => CHtml::listData(Firm::model()->findAll(), 'id', 'name_firm'),
                'htmlOptions' => array('prompt'=>'(выберите фирму)'),
            ),
        )
    ); ?>

    <?php echo $form->dropDownListGroup(
        $model,
        'id_position',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'widgetOptions' => array(
                'data' => CHtml::listData(Position::model()->findAll(), 'id', 'name_position'),
                'htmlOptions' => array('prompt'=>'(выберите должность)'),
            ),
        )
    ); ?>

    <?php echo $form->dropDownListGroup(
        $model,
        'sex',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'widgetOptions' => array(
                'data' => array('0' => 'мужской', '1' => 'женский'),
                'htmlOptions' => array('prompt'=>'(выберите пол)'),
            ),
        )
    ); ?>

    <div class="row">
        <div id='contact'></div>
        <script>
            ContactOptions = <?php echo $ContactSchema; ?>;
            var contact_form = new JSONEditor(document.getElementById('contact'), ContactOptions);
        </script>
    </div>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'buttonType'=>'submit',
            'context'=>'primary',
            'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->