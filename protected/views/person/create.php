<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'Персоны'=>array('index'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список персон', 'url'=>array('index')),
	array('label'=>'Управление персонами', 'url'=>array('admin')),
);
?>

<h1>Создать персону</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>