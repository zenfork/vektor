<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'Персоны'=>array('index'),
	$model->getFullName()=>array('view','id'=>$model->id),
	'Обновить',
);

$this->menu=array(
	array('label'=>'Список персон', 'url'=>array('index')),
	array('label'=>'Создать персону', 'url'=>array('create')),
	array('label'=>'Просмотр персоны', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление персонами', 'url'=>array('admin')),
);
?>

<h1>Обновить персону "<?php echo $model->getFullName(); ?>"</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>