<?php
/**
 * Created by PhpStorm.
 * User: Ura
 * Date: 21.04.2016
 * Time: 12:33
 */
foreach ($model->contacts as $contact)
{
    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$contact,
        'attributes'=>array(
            array('label'=>$contact->kind_connect->kind_connect, 'value'=>$contact->communication_val),
        ),
    ));
}
if (empty($model->contacts)) echo '<span class="null">Пусто</span>';