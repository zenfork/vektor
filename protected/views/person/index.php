<?php
/* @var $this PersonController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Персоны',
);

$this->menu=array(
	array('label'=>'Создать персону', 'url'=>array('create')),
	array('label'=>'Управление персонами', 'url'=>array('admin')),
);
?>

<h1>Персоны</h1>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/
$columns=array
(
    array( 'name'=>'fullname', 'value'=>'$data->getFullName()' ),
    array(
        'name'=>'id_position',
        'value'=>'$data->position->name_position',
        'filter' => CHtml::listData(Position::model()->findAll(),'id','name_position'),
         ),
    array( 'name'=>'id_firm', 'value'=>'$data->firm->name_firm' ),
);

if(Yii::app()->user->checkAccess('Person.View') && !Yii::app()->user->checkAccess('Person.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn',
        'template'=>'{view}'
    );
}
if(Yii::app()->user->checkAccess('Person.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn'
    );
}

$this->widget('ext.yiibooster.widgets.TbExtendedGridView', array(
    'filter'=>$model,
    'type'=>'striped bordered',
    'dataProvider' => $model->search(),
    'template' => "{summary}{pager}{items}{pager}",
    'columns'=> $columns
    /*array(
        //	'id',
        array( 'name'=>'fullname', 'value'=>'$data->getFullName()' ),
        array( 'name'=>'position_search', 'value'=>'$data->position->name_position' ),
        array( 'name'=>'firm_search', 'value'=>'$data->firm->name_firm' ),
        //post strictly required user but in any case that model has not required user(or another relation) you should replace with it
        'sex',
        array(
            'class'=>'ext.yiibooster.widgets.TbButtonColumn',
        ),
    ),*/
));
?>
