<?php
/* @var $this ProductController */
/* @var $model Product */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/jsoneditor.min.js');
$StoreSchema = json_encode(Store::getEditorSchema());
?>
<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'product-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model,'name_product',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'.col-md-4','maxlength'=>255)))); ?>

    <div class="row">
        <div id='store'></div>
        <script>
            StoreOptions = <?php echo $StoreSchema; ?>;
            var store_form = new JSONEditor(document.getElementById('store'), StoreOptions);
        </script>
    </div>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'buttonType'=>'submit',
            'context'=>'primary',
            'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->