<?php
/**
 * Created by PhpStorm.
 * User: Ura
 * Date: 21.04.2016
 * Time: 16:33
 */
foreach ($model->stores as $store)
{
    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$store,
        'attributes'=>array(
            array('label'=>CHtml::link(CHtml::encode($store->subproduct->name_subproduct), array('subproduct/view', 'id'=>$store->subproduct->id)), 'value'=>$store->number.' шт.'),
        ),
    ));
}
if (empty($model->stores)) echo '<span class="null">Пусто</span>';