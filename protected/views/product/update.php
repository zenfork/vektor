<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Продукты'=>array('index'),
	$model->name_product=>array('view','id'=>$model->id),
	'Обновить',
);

$this->menu=array(
	array('label'=>'Список продуктов', 'url'=>array('index')),
	array('label'=>'Создать продукт', 'url'=>array('create')),
	array('label'=>'Просмотр продукта', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление продуктами', 'url'=>array('admin')),
);
?>

<h1>Обновить продукт "<?php echo $model->name_product; ?>"</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>