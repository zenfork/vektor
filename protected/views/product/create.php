<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Продукты'=>array('index'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список продуктов', 'url'=>array('index')),
	array('label'=>'Управление продуктами', 'url'=>array('admin')),
);
?>

<h1>Создать продукт</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>