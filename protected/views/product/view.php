<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Продукты'=>array('index'),
	$model->name_product,
);

$this->menu=array(
	array('label'=>'Список продуктов', 'url'=>array('index')),
	array('label'=>'Создать продукт', 'url'=>array('create')),
	array('label'=>'Обновить продукт', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить продукт', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление продуктами', 'url'=>array('admin')),
);
?>

<h1>Просмотр продукта "<?php echo $model->name_product; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name_product',
        array(
            'label'=>'Субпродукты',
            'type'=>'raw',
            'value'=>$this->renderPartial('_subproducts', array('model'=>$model), true),
        ),
	),
)); ?>
