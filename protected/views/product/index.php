<?php
/* @var $this ProductController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Продукты',
);

$this->menu=array(
	array('label'=>'Создать продукт', 'url'=>array('create')),
	array('label'=>'Управление продуктами', 'url'=>array('admin')),
);
?>

<h1>Продукты</h1>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/

/*$this->widget(
    'ext.yiibooster.widgets.TbExtendedGridView',
    array(
        'fixedHeader' => true,
        'headerOffset' => 40,
        // 40px is the height of the main navigation at bootstrap
        'type' => 'striped',
        'dataProvider' => $dataProvider,
        'responsiveTable' => true,
        'template' => "{items}",
        'columns'=>array(
            'id',
            'name_product',
            array(
                'class'=>'CButtonColumn',
            ),
        ),
    )
);*/
$columns=array
(
);

if(Yii::app()->user->checkAccess('Product.View') && !Yii::app()->user->checkAccess('Product.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn',
        'template'=>'{view}'
    );
}
if(Yii::app()->user->checkAccess('Product.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn'
    );
}

$this->widget('ext.yiibooster.widgets.TbExtendedGridView', array(
    'filter'=>$model,
    'type'=>'striped bordered',
    'dataProvider' => $model->search(),
    'template' => "{summary}{pager}{items}{pager}",
    'columns' => array_merge(array(
        array(
            'class'=>'ext.yiibooster.widgets.TbRelationalColumn',
            'name'=>'name_product',
            'url' => $this->createUrl('store/rsubproduct'),
            'value'=> '$data->name_product'
        )
    ),
        $columns
   /*array(//'id',
    array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn',
        ),
    )*/
    ),
));
?>
