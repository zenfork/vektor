<?php
/* @var $this SubproductController */
/* @var $data Subproduct */

/*$this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'product-grid-'.$id,
            'dataProvider'=>$gridDataProvider,
            'filter'=>$model,
            'columns'=>array(
                'product.id',
                'product.name_product',
                'specifications.number',
                array(
                    'class'=>'CButtonColumn',
                ),
            ),
    ));*/
$columns=
    array(
        'id',
        array(
            'name'=>'Штамп',
            'type'=>'raw',
            'value' => 'CHtml::link($data->stamp,Yii::app()->createUrl("project/view",array("id"=>$data->id)))',
        ),
    );

if(Yii::app()->user->checkAccess('Person.View') && !Yii::app()->user->checkAccess('Person.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn',
        'template'=>'{view}'
    );
}
if(Yii::app()->user->checkAccess('Person.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn'
    );
}
$this->widget('ext.yiibooster.widgets.TbExtendedGridView', array(
    'type'=>'striped bordered',
    'dataProvider'=>$gridDataProvider,
    'template' => "{summary}{pager}{items}{pager}",
    'columns' => /*array_merge(array(
        array(
            'class'=>'ext.yiibooster.widgets.TbRelationalColumn',
            'name' => 'firstLetter',
            'url' => $this->createUrl('subproduct/relational'),
            'value'=> '"subgrid"'
        )
    ),*/
        $columns
/*        array(
        //    'id',
            'stamp',
            array('name'=>'person_responsible', 'value'=>'$data->responsible->FullName'),
            array('name'=>'person_designer', 'value'=>'$data->designer->FullName'),
            array(
                'class'=>'ext.yiibooster.widgets.TbButtonColumn',
                 ),
        ),*/
));
?>