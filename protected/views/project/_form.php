<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form CActiveForm */

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/jsoneditor.min.js');
$SpecificationSchema = json_encode(Specification::getEditorSchema());
?>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'project-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'), // FOR UPLOADING FILES
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <?php echo $form->dropDownListGroup(
        $model,
        'id_object',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'widgetOptions' => array(
                'data' => CHtml::listData(Object::model()->findAll(), 'id', 'name_object'),
                'htmlOptions' => array('prompt'=>'(выберите объект)'),
            ),
        )
    ); ?>

    <?php echo $form->textFieldGroup($model,'stamp',array(
        'widgetOptions'=>array(
            'htmlOptions'=>array('class'=>'.col-md-4','maxlength'=>32)
        ))
    ); ?>

    <?php echo $form->dropDownListGroup(
        $model,
        'status',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'widgetOptions' => array(
                'data' => array('0' => 'Р', '1' => 'Н'),
                'htmlOptions' => array('prompt'=>'(выберите статус)'),
            ),
        )
    ); ?>

    <?php echo $form->dropDownListGroup(
        $model,
        'id_responsible',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'widgetOptions' => array(
                'data' => CHtml::listData(Person::model()->findAll(array('order'=>'CONCAT_WS(" ", first_name, last_name, surname) ASC')), 'id', 'fullName'),
                'htmlOptions' => array('prompt'=>'(выберите статус)'),
            ),
        )
    ); ?>

    <?php echo $form->dropDownListGroup(
        $model,
        'id_designer',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'widgetOptions' => array(
                'data' => CHtml::listData(Person::model()->findAll(array('order'=>'CONCAT_WS(" ", first_name, last_name, surname) ASC')), 'id', 'fullName'),
                'htmlOptions' => array('prompt'=>'(выберите статус)'),
            ),
        )
    ); ?>

    <?php echo $form->dropDownListGroup(
        $model,
        'redesign',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
),
            'widgetOptions' => array(
                'data' => array('0' => 'нет', '1' => 'да'),
                'htmlOptions' => array('prompt'=>'(выберите статус перепроектирования)'),
            ),
        )
    );
    // echo $form->checkBox($model, 'redesign');
    ?>

    <?php echo $form->dropDownListGroup(
        $model,
        'id_firm',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'widgetOptions' => array(
                'data' => CHtml::listData(Firm::model()->findAll(), 'id', 'name_firm'),
                'htmlOptions' => array('prompt'=>'(выберите фирму)'),
            ),
        )
    );

$this->widget('booster.widgets.TbExtendedGridView', array(
    'type'=>'striped bordered',
    'dataProvider'=>new CArrayDataProvider($model->project_files, array()),
    'template' => "{summary}{pager}{items}{pager}",
    'columns' =>
        array(
            'id',
            array( 'class'=>'CLinkColumn',
                'header'=>'Имя файла',
                'labelExpression'=>'$data->name_file',
                'urlExpression'=>'Yii::app()->createUrl("/documents/project/".$data->id_project."/".$data->name_file)',
                'linkHtmlOptions'=>array('title'=>'Нажмите, чтобы открыть файл', "target"=>"_blank")
            ),
            array(
                'template'=>'{view} {delete}',
                'class'=>'booster.widgets.TbButtonColumn',
                'viewButtonUrl'=>'Yii::app()->createUrl("/projectfile/view", array("name_file" => $data->id))',
                'deleteButtonUrl'=>'Yii::app()->createUrl("/projectfile/delete", array("id" => $data->id))',
                'updateButtonUrl'=>'Yii::app()->createUrl("/projectfile/update", array("id" => $data->id))',
            ),
        ),
));

    $this->widget('CMultiFileUpload', array(
        'name'=>'file',
    //  'accept' => 'jpeg|jpg|gif|png',
        'duplicate' => 'Такой файл уже имеется!',
        'denied' => 'Неверный формат файла',
    ));
    ?>

    <div class="row">
        <div id='specification'></div>
        <script>
            SpecificationOptions = <?php echo $SpecificationSchema; ?>;
            var specification_form = new JSONEditor(document.getElementById('specification'), SpecificationOptions);
        </script>
    </div>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'buttonType'=>'submit',
            'context'=>'primary',
            'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->