<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
	'Проекты'=>array('index'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список проектов', 'url'=>array('index')),
	array('label'=>'Управление проектами', 'url'=>array('admin')),
);
?>

<h1>Создать проект</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>