<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
	'Проекты'=>array('index'),
	$model->stamp,
);

$this->menu=array(
	array('label'=>'Список проектов', 'url'=>array('index')),
	array('label'=>'Создать проект', 'url'=>array('create')),
	array('label'=>'Обновить проект', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить проект', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление проектами', 'url'=>array('admin')),
);
?>

<h1>Просмотр проекта "<?php echo $model->stamp; ?>"</h1>

<?php
/*
    switch ($model->status) {
    case "0":
        $status = 'Р';
        break;
    case "1":
        $status = 'Н';
        break;
}*/

$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
	//	'id',
		'object.name_object',
        'object.address_object',
		'stamp',
        [
            'name' => 'status',
        //  'format'=>'raw',
            'value' => $model->getStatus($model->status)
        ],
        array('name'=>'id_responsible', 'value'=>$model->responsible->FullName), //	'responsible.fullName',
        // 'label'=>$model->responsible->getAttributeLabel('fullname')
        array('name'=>'id_designer', 'value'=>$model->designer->FullName), //  'designer.fullName',
        [
            'name' => 'redesign',
        //  'format'=>'raw',
            'value' => $model->getRedesign($model->redesign)
        ],
		'firm.name_firm',
        array(
            'label'=>'Продукты',
            'type'=>'raw',
            'value'=>$this->renderPartial('_products', array('model'=>$model), true),
        ),
        array(
            'label'=>'Файлы',
            'type'=>'raw',
            'value'=>$this->renderPartial('_files', array('model'=>$model), true),
        ),
	),
));/*
var_dump($model->specifications);
var_dump($model->product);
var_dump($model->stores);
var_dump($model->subproduct);*/
?>
