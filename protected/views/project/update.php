<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
	'Проекты'=>array('index'),
	$model->stamp=>array('view','id'=>$model->id),
	'Обновить',
);

$this->menu=array(
	array('label'=>'Список проектов', 'url'=>array('index')),
	array('label'=>'Создать проект', 'url'=>array('create')),
	array('label'=>'Просмотр проекта', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление проектами', 'url'=>array('admin')),
);
?>

<h1>Обновить проект "<?php echo $model->stamp; ?>"</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>