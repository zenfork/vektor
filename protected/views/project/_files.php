<?php
/**
 * Created by PhpStorm.
 * User: Ura
 * Date: 21.04.2016
 * Time: 16:33
 */
foreach ($model->project_files as $project_file)
{
    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$project_file,
        'attributes'=>array(
            array(
                    'label' => $project_file->id,
                    'type' => 'raw',
                    'value' => CHtml::link(CHtml::encode($project_file->name_file),
                        Yii::app()->baseUrl.'/documents/project/' . $model->id . '/' . $project_file->name_file, array("target"=>"_blank"))
            )
        ),
    ));
}
if (empty($model->project_files)) echo '<span class="null">Пусто</span>';