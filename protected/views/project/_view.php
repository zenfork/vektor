<?php
/* @var $this ProjectController */
/* @var $data Project */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_object')); ?>:</b>
	<?php echo CHtml::encode($data->id_object); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stamp')); ?>:</b>
	<?php echo CHtml::encode($data->stamp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_responsible')); ?>:</b>
	<?php echo CHtml::encode($data->id_responsible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_designer')); ?>:</b>
	<?php echo CHtml::encode($data->id_designer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('redesign')); ?>:</b>
	<?php echo CHtml::encode($data->redesign); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('id_firm')); ?>:</b>
	<?php echo CHtml::encode($data->id_firm); ?>
	<br />

	*/ ?>

</div>