<?php
/**
 * Created by PhpStorm.
 * User: Ura
 * Date: 21.04.2016
 * Time: 16:33
 */
foreach ($model->specifications as $specification)
{
    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$specification,
        'attributes'=>array(
            array('label'=>CHtml::link(CHtml::encode($specification->product->name_product), array('product/view', 'id'=>$specification->product->id)), 'value'=>$specification->number.' шт.'),
        ),
    ));
}
if (empty($model->specifications)) echo '<span class="null">Пусто</span>';