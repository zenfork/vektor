<?php
/* @var $this ProjectController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Проекты',
);

$this->menu=array(
	array('label'=>'Создать проект', 'url'=>array('create')),
	array('label'=>'Управление проектами', 'url'=>array('admin')),
);
?>

<h1>Проекты</h1>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/
$columns=array
(
    array('name'=>'id_object', 'value'=>'$data->object->name_object'),
    array('name'=>'address_object', 'value'=>'$data->object->address_object'),
    array('name'=>'id_responsible', 'value'=>'$data->responsible->FullName'),
    array('name'=>'id_designer', 'value'=>'$data->designer->FullName'),
    array('name'=>'id_firm', 'value'=>'$data->firm->name_firm'),
    array(
        'name'=>'id_territory',
        'value'=>'$data->territory->name_territory',
        'filter' => CHtml::listData(Territory::model()->findAll(array('order' => 'name_territory')),'id','name_territory'),
    ),
//  'redesign:boolean',
/*    array(
        'name' => 'redesign',
    //  'type' => 'boolean',
        'value' => '$data->getRedesign($data->redesign)',
        'filter' => array(''=>'все', 0=> 'нет', 1 => 'да'),
    ),*/
    array(
        'class'=>'ext.yiibooster.widgets.TbToggleColumn',
        'toggleAction' => 'project/toggleredesign',
        'name'=>'redesign',
        'filter' => array(''=>'все', 0=> 'нет', 1 => 'да'),
    ),
    array(
        'name' => 'status',
    //  'type' => 'raw',
        'value' => '$data->getStatus($data->status)',
        'filter' => array(''=>'все', 0 => 'Р', 1=> 'Н'),
    ),
/*    array(
         'name' => 'status',
         'header' => "Status",
         'value' => '$data->status?Yii::t(\'app\',\'Yes\'):Yii::t(\'app\', \'No\')',
         'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes'), '' => Yii::t('app', 'null')),
         'htmlOptions' => array('style' => "text-align:center;"),
    ),*/
);

if(Yii::app()->user->checkAccess('Project.View') && !Yii::app()->user->checkAccess('Project.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn',
        'template'=>'{view}'
    );
}
if(Yii::app()->user->checkAccess('Project.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn'
    );
}

$this->widget('ext.yiibooster.widgets.TbExtendedGridView', array(
    'filter'=>$model,
    'id'=>'project-grid-'.$id,
    'type'=>'striped bordered',
    'dataProvider' => $model->search(),
    'template' => "{summary}{pager}{items}{pager}",
    'columns' => array_merge(array(
        array(
            'class'=>'ext.yiibooster.widgets.TbRelationalColumn',
            'htmlOptions'=>array('class'=>'tbrelational-column-project'),
            'name'=>'stamp',
            'url' => $this->createUrl('specification/relational'),
            'value'=> '$data->stamp'
        )
    ),
    $columns
    ),
));
?>
