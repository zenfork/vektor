<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Вход';
$this->breadcrumbs=array(
	'Вход',
);
?>

<h1>Вход</h1>

<p>Пожалуйста, заполните следующую форму с вашими учетными данными для входа:</p>

<div class="form">
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
    'htmlOptions' => array('class' => 'well'), // for inset effect
)); ?>

	<p class="note">Поля обозначены <span class="required">*</span> обязательны для заполнения.</p>

    <?php echo $form->textFieldGroup($model,'username',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'.col-md-4')))); ?>

    <?php echo $form->passwordFieldGroup($model,'password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'.col-md-4')))); ?>

    <?php echo $form->checkboxGroup($model,'rememberMe'); ?>

    <?php
    $this->widget(
    'booster.widgets.TbButton',
    array('buttonType' => 'submit', 'label' => 'Войти')
    );
    ?>

<?php $this->endWidget(); ?>
</div><!-- form -->
