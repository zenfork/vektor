<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Добро пожаловать в закрытую систему ведения учёта "<i><?php echo CHtml::encode(Yii::app()->name); ?></i>"</h1>

<?php if (Yii::app()->user->isGuest) { ?>
    <p>Если Вы прошли <?php echo CHtml::link('регистрацию',array('user/registration')); ?> и Вам были наданы права для просмотра данных, то можете использовать <?php echo CHtml::link('страницу входа',array('site/login')); ?>, чтобы войти в систему.</p>
<?php } else { ?>

<p>Для перехода по таблицам используйте меню навигации вверху страницы.</p>

<?php } ?>
<p>Возникли вопросы? Воспользуйтесь <?php echo CHtml::link('формой обратной связи',array('site/contact')); ?>.</p>