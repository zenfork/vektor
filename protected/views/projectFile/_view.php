<?php
/* @var $this ProjectFileController */
/* @var $data ProjectFile */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_project')); ?>:</b>
	<?php echo CHtml::encode($data->id_project); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_file')); ?>:</b>
	<?php echo CHtml::encode($data->name_file); ?>
	<br />


</div>