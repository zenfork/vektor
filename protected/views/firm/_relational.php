<?php
/* @var $this SubproductController */
/* @var $data Subproduct */

$columns=array
    (
  //   'id',
        array(
            'name'=>'Фирма',
            'value'=>'$data->name_firm'
        ),
    );

if(Yii::app()->user->checkAccess('Object.View') && !Yii::app()->user->checkAccess('Object.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn',
        'template'=>'{view}'
    );
}
if(Yii::app()->user->checkAccess('Object.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn'
    );
}
$this->widget('ext.yiibooster.widgets.TbExtendedGridView', array(
    'id'=>'territory-grid-'.$id,
    'type'=>'striped bordered',
    'dataProvider'=>$gridDataProvider,
    'template' => "{summary}{pager}{items}{pager}",
    'columns' => $columns
    /*   array(
           'id',
           'name_firm',
           array(
               'class'=>'ext.yiibooster.widgets.TbButtonColumn',
           ),
       ),*/
));
?>