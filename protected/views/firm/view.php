<?php
/* @var $this FirmController */
/* @var $model Firm */

$this->breadcrumbs=array(
	'Фирмы'=>array('index'),
	$model->name_firm,
);

$this->menu=array(
	array('label'=>'Список фирм', 'url'=>array('index')),
	array('label'=>'Создать фирму', 'url'=>array('create')),
	array('label'=>'Обновить фирму', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить фирму', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление фирмами', 'url'=>array('admin')),
);
?>

<h1>Просмотр фирмы "<?php echo $model->name_firm; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name_firm',
		'dealer:boolean',
        array(
            'name'=>'name_territory',
            'value'=>$model->territory->name_territory,
        ),
		'firm_address',
		'edrpou',
	),
)); ?>
