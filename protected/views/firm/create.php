<?php
/* @var $this FirmController */
/* @var $model Firm */

$this->breadcrumbs=array(
	'Фирмы'=>array('index'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список фирм', 'url'=>array('index')),
	array('label'=>'Управление фирмами', 'url'=>array('admin')),
);
?>

<h1>Создать фирму</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>