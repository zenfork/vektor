<?php
/* @var $this FirmController */
/* @var $data Firm */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_firm')); ?>:</b>
	<?php echo CHtml::encode($data->name_firm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dealer')); ?>:</b>
	<?php echo CHtml::encode($data->dealer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_territory')); ?>:</b>
	<?php echo CHtml::encode($data->id_territory); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('firm_address')); ?>:</b>
	<?php echo CHtml::encode($data->firm_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('edrpou')); ?>:</b>
	<?php echo CHtml::encode($data->edrpou); ?>
	<br />


</div>