
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
    'id'=>'firm-form',
    'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldGroup($model,'name_firm',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'.col-md-4','maxlength'=>80)))); ?>

<?php echo $form->dropDownListGroup(
    $model,
    'dealer',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'widgetOptions' => array(
            'data' => array('0' => 'нет', '1' => 'да'),
            'htmlOptions' => array('prompt'=>'(выберите тип)'),
        ),
    )
); ?>

<?php echo $form->dropDownListGroup(
    $model,
    'id_territory',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'widgetOptions' => array(
            'data' => CHtml::listData(Territory::model()->findAll(), 'id', 'name_territory'),
            'htmlOptions' => array('prompt'=>'(выберите тип)'),
        ),
    )
); ?>

<?php echo $form->textFieldGroup($model,'firm_address',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'.col-md-4','maxlength'=>255)))); ?>

<?php echo $form->textFieldGroup($model,'edrpou',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'.col-md-4','maxlength'=>40)))); ?>

<div class="form-actions">
    <?php $this->widget('booster.widgets.TbButton', array(
        'buttonType'=>'submit',
        'context'=>'primary',
        'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
    )); ?>
</div>

<?php $this->endWidget(); ?>