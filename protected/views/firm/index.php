<?php
/* @var $this FirmController */
/* @var $dataProvider CActiveDataProvider */

//$this->pageTitle = "Фирмы";
$this->breadcrumbs=array(
	'Фирмы',
);

$this->menu=array(
	array('label'=>'Создать фирму', 'url'=>array('create')),
	array('label'=>'Управление фирмами', 'url'=>array('admin')),
);
?>

<h1>Фирмы</h1>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); */

$columns=array(
    array(
        'class'=>'ext.yiibooster.widgets.TbToggleColumn',
        'toggleAction' => 'firm/toggledealer',
         'name'=>'dealer',
        'filter' => array(''=>'все', 1 => 'дилер', 0=> 'не дилер'),
    ),
    array(
        'name'=>'id_territory',
        'value'=>'$data->territory->name_territory',
        'filter' => CHtml::listData(Territory::model()->findAll(array('order' => 'name_territory')),'id','name_territory'),
    ),
    array(
        'name'=>'firm_address',
        'value'=>'$data->firm_address',
    ),
    array(
        'name'=>'edrpou',
        'value'=>'$data->edrpou',
    ));
//if(!Yii::app()->user->isGuest) {
if(Yii::app()->user->checkAccess('Firm.View') && !Yii::app()->user->checkAccess('Firm.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn',
        'template'=>'{view}'
    );
}
if(Yii::app()->user->checkAccess('Firm.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn'
    );
}
$this->widget('ext.yiibooster.widgets.TbExtendedGridView', array(
    'filter'=>$model,
    'type'=>'striped bordered',
    'dataProvider' => $model->search(),
    'template' => "{summary}{pager}{items}{pager}",
    'columns' => array_merge(array(
        array(
            'class'=>'ext.yiibooster.widgets.TbRelationalColumn',
            'name' => 'name_firm',
            'url' => $this->createUrl('project/relational'),
            'value'=> '$data->name_firm'
        )
    ),
            $columns
       /*     'dealer',
            array( 'name'=>'name_territory', 'value'=>'$data->territory->name_territory' ),
            'firm_address',
            'edrpou',*/

    ),
));
?>
