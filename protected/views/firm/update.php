<?php
/* @var $this FirmController */
/* @var $model Firm */

$this->breadcrumbs=array(
	'Фирмы'=>array('index'),
	$model->name_firm=>array('view','id'=>$model->id),
	'Обновить',
);

$this->menu=array(
	array('label'=>'Список фирм', 'url'=>array('index')),
	array('label'=>'Создать фирму', 'url'=>array('create')),
	array('label'=>'Просмотр фирмы', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление фирмами', 'url'=>array('admin')),
);
?>

<h1>Обновить фирму "<?php echo $model->name_firm; ?>"</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>