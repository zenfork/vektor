<?php
/* @var $this SubproductController */
/* @var $model Subproduct */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'subproduct-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model,'name_subproduct',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'.col-md-4','maxlength'=>80)))); ?>

    <?php echo $form->textAreaGroup($model,'full_name_subproduct',array(
        'widgetOptions'=>array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
                ),
            'htmlOptions' => array('rows' => 5)
        )
    )); ?>

    <div>
        <?php // Вывод уже загруженной картинки или изображения No_photo
        echo $this->post_image($model->id, $model->image, '150','small_img left');?>
        <br clear="all">
        <?php //Если картинка для данного товара загружена, предложить её удалить, отметив чекбокс
        if(isset($model->image) && file_exists(Yii::getPathOfAlias('webroot').'/images/subproduct/'.$model->image)){
            echo $form->checkBox($model,'del_img',array('class'=>'span-1'));
            echo $form->labelEx($model,'del_img',array('class'=>'span-2'));
        }?>
        <br />
        <?php echo $form->fileFieldGroup($model, 'icon',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
            )
        ); ?>

    </div>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'buttonType'=>'submit',
            'context'=>'primary',
            'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить'
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->