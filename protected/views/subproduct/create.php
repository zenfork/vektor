<?php
/* @var $this SubproductController */
/* @var $model Subproduct */

$this->breadcrumbs=array(
	'Подпродукты'=>array('index'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список подпродуктов', 'url'=>array('index')),
	array('label'=>'Управление подпродуктами', 'url'=>array('admin')),
);
?>

<h1>Создать подпродукт</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>