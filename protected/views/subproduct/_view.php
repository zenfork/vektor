<?php
/* @var $this SubproductController */
/* @var $data Subproduct */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_subproduct')); ?>:</b>
	<?php echo CHtml::encode($data->name_subproduct); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('full_name_subproduct')); ?>:</b>
	<?php echo CHtml::encode($data->full_name_subproduct); ?>
	<br />

    <?php echo $this->post_image($data->name_subproduct, $data->image, '150','small_img left');?>

</div>