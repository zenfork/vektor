<?php
/* @var $this SubproductController */
/* @var $model Subproduct */

$this->breadcrumbs=array(
	'Подпродукты'=>array('index'),
	$model->name_subproduct,
);

$this->menu=array(
	array('label'=>'Список подпродуктов', 'url'=>array('index')),
	array('label'=>'Создать подпродукт', 'url'=>array('create')),
	array('label'=>'Обновить подпродукт', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить подпродукт', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление подпродуктами', 'url'=>array('admin')),
);
?>

<h1>Просмотр подпродукта "<?php echo $model->name_subproduct; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name_subproduct',
		'full_name_subproduct',
        array(
            'label'=>'Изображение',
         //   'type'=>'image',
         //   'value'=> $model->getImageThumb($model->name_subproduct, $model->image),
             'type'=>'raw',
             'value'=> $this->post_image($model->name_subproduct, $model->image, '680','small_img left'),
        ),
	),
)); ?>
