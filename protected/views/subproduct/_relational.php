<?php
/* @var $this SubproductController */
/* @var $data Subproduct */
?>
<?php
$this->widget('ext.yiibooster.widgets.TbExtendedGridView', array(
    'id'=>'subproduct-grid-'.$id,
    'type'=>'striped bordered',
    'dataProvider'=>$gridDataProvider,
    'template' => "{items}",
    'columns' =>
        array(
            'id',
            array('name'=>'name_subproduct', 'value'=>'$data->subproduct->name_subproduct'),
            'name_subproduct',
            'full_name_subproduct',
            array(
                'class'=>'ext.yiibooster.widgets.TbButtonColumn'
            ),
        ),
));
?>