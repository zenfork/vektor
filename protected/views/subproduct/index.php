<?php
/* @var $this SubproductController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Подпродукты',
);

$this->menu=array(
	array('label'=>'Создать подпродукт', 'url'=>array('create')),
	array('label'=>'Управление подпродуктами', 'url'=>array('admin')),
);
?>

<h1>Подпродукты</h1>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/
$columns=array
(
    array( 'class'=>'booster.widgets.TbImageColumn',
        'imagePathExpression'=>'$data->getImageThumb($data->name_subproduct, $data->image)',
        'headerHtmlOptions'=>array('style'=>'min-width: 50px;'),
        'imageOptions'=>array('width'=>'50px'),
    ),
    array(
        'class'=>'ext.yiibooster.widgets.TbRelationalColumn',
        'name'=>'name_subproduct',
        'url' => $this->createUrl('store/rproduct'),
        'value'=> '$data->name_subproduct'
    ),
    'full_name_subproduct'
);

if(Yii::app()->user->checkAccess('Subproduct.View') && !Yii::app()->user->checkAccess('Subproduct.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn',
        'template'=>'{view}'
    );
}
if(Yii::app()->user->checkAccess('Subproduct.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn'
    );
}

$this->widget('ext.yiibooster.widgets.TbExtendedGridView', array(
    'filter'=>$model,
    'type'=>'striped bordered',
    'dataProvider' => $model->search(),
    'template' => "{summary}{pager}{items}{pager}",
    'columns' => $columns
));
?>