<?php
/* @var $this SubproductController */
/* @var $model Subproduct */

$this->breadcrumbs=array(
	'Подпродукты'=>array('index'),
	$model->name_subproduct=>array('view','id'=>$model->id),
	'Обновить',
);

$this->menu=array(
	array('label'=>'Список подпродуктов', 'url'=>array('index')),
	array('label'=>'Создать подпродукт', 'url'=>array('create')),
	array('label'=>'Просмотр подпродукта', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление подпродуктами', 'url'=>array('admin')),
);
?>

<h1>Обновить подпродукт "<?php echo $model->name_subproduct; ?>"</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>