<?php
/* @var $this ObjectController */
/* @var $data Object */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_object')); ?>:</b>
	<?php echo CHtml::encode($data->name_object); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type_object')); ?>:</b>
	<?php echo CHtml::encode($data->type_object); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desc_object')); ?>:</b>
	<?php echo CHtml::encode($data->desc_object); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address_object')); ?>:</b>
	<?php echo CHtml::encode($data->address_object); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_developer')); ?>:</b>
	<?php echo CHtml::encode($data->id_developer); ?>
	<br />


</div>