<?php
/* @var $this ObjectController */
/* @var $model Object */

$this->breadcrumbs=array(
	'Объекты'=>array('index'),
	$model->name_object=>array('view','id'=>$model->id),
	'Обновить',
);

$this->menu=array(
	array('label'=>'Список объектов', 'url'=>array('index')),
	array('label'=>'Создать объект', 'url'=>array('create')),
	array('label'=>'Просмотр объекта', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление объектами', 'url'=>array('admin')),
);
?>

<h1>Обновить объект "<?php echo $model->name_object; ?>"</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>