<?php
/* @var $this ObjectController */
/* @var $model Object */

$this->breadcrumbs=array(
	'Объекты'=>array('index'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список объектов', 'url'=>array('index')),
	array('label'=>'Управление объектами', 'url'=>array('admin')),
);
?>

<h1>Create Object</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>