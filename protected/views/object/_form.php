<?php
/* @var $this ObjectController */
/* @var $model Object */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'object-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model,'name_object',array('widgetOptions'=>array(
        'htmlOptions'=>array('class'=>'.col-md-4','size'=>60,'maxlength'=>255)))); ?>

    <?php echo $form->textFieldGroup($model,'type_object',array('widgetOptions'=>array(
        'htmlOptions'=>array('class'=>'.col-md-4','size'=>60,'maxlength'=>100)))); ?>

    <?php echo $form->textAreaGroup($model,'desc_object',array('widgetOptions'=>array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'htmlOptions' => array('rows' => 5)
    ))); ?>

    <?php echo $form->textFieldGroup($model,'address_object',array('widgetOptions'=>array(
        'htmlOptions'=>array('class'=>'.col-md-4','size'=>60,'maxlength'=>255)))); ?>

    <?php echo $form->dropDownListGroup(
        $model,
        'id_developer',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'widgetOptions' => array(
                'data' => CHtml::listData(Firm::model()->findAll(), 'id', 'name_firm'),
                'htmlOptions' => array('prompt'=>'(выберите фирму)'),
            ),
        )
    ); ?>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'buttonType'=>'submit',
            'context'=>'primary',
            'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
        )); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->