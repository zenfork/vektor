<?php
/* @var $this ObjectController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Объекты',
);

$this->menu=array(
	array('label'=>'Создать объект', 'url'=>array('create')),
	array('label'=>'Управление объектами', 'url'=>array('admin')),
);
?>

<h1>Объекты</h1>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/
$columns=array(
    array(
        'name'=>'type_object',
        'value'=>'$data->type_object',
    ),
    array(
        'name'=>'desc_object',
        'value'=>'$data->desc_object',
    ),
    array(
        'name'=>'address_object',
        'value'=>'$data->address_object',
    ),

array(
    'name'=>'firm_search',
    'value'=>'$data->developer->name_firm',
    'filter' => CHtml::listData(Firm::model()->findAll(array('order' => 'name_firm')),'id','name_firm'),
));
//if(!Yii::app()->user->isGuest) {
if(Yii::app()->user->checkAccess('Object.View') && !Yii::app()->user->checkAccess('Object.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn',
        'template'=>'{view}'
    );
}
if(Yii::app()->user->checkAccess('Object.Edit')) {
    $columns[]=array(
        'class'=>'ext.yiibooster.widgets.TbButtonColumn'
    );
}

$this->widget('ext.yiibooster.widgets.TbExtendedGridView', array(
    'filter'=>$model,
    'type'=>'striped bordered',
    'dataProvider' => $model->search(),
    'template' => "{summary}{pager}{items}{pager}",
    'columns' => array_merge(array(
        array(
            'class'=>'ext.yiibooster.widgets.TbRelationalColumn',
            'name' => 'name_object',
            'url' => $this->createUrl('project/relational'),
            'value'=> '$data->name_object'
        )
    ),
        $columns
    /*       array(
               'id',
               'type_object',
               'desc_object',
               'address_object',
               array(
                   'class'=>'ext.yiibooster.widgets.TbButtonColumn',
               ),
        )*/
    ),
));
?>
