<?php
/* @var $this ObjectController */
/* @var $model Object */

$this->breadcrumbs=array(
	'Объекты'=>array('index'),
	$model->name_object,
);

$this->menu=array(
	array('label'=>'Список объектов', 'url'=>array('index')),
	array('label'=>'Создать объект', 'url'=>array('create')),
	array('label'=>'Обновить объект', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить объект', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление объектами', 'url'=>array('admin')),
);
?>

<h1>Просмотр объекта "<?php echo $model->name_object; ?>"</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name_object',
		'type_object',
		'desc_object',
		'address_object',
		'developer.name_firm',
	),
)); ?>
